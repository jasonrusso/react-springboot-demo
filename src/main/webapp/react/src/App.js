import React from 'react';
import logo from './logo.svg';
import './App.css';
import HelloWorldCard from './HelloWorldCard.js'

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Spring Boot + React Starter App</h1>
          <HelloWorldCard/>
        </header>
      </div>
    );
  }
}

export default App;