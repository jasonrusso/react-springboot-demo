import React from 'react';

class HelloWorldCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null
        }
    }

    componentDidMount() {
        fetch('/api').then(
            async resp => {
                let respObj = await resp.json();
                this.setState({ data: {
                    param1: respObj.data1,
                    param2: respObj.data2,
                    param3: respObj.data3
                } });

                console.log(respObj);
            }
        )
    }

    render() {
        return (
            <div className="App-body">
                Hello World Card has a message:<br/>
                <h2>{this.state.data != null ? this.state.data.param1 + ' ' + this.state.data.param2
                        : 'Oops... Having trouble with the api service... :(' }</h2>
                <h3>The time is: {this.state.data != null ? this.state.data.param3 : "N/A"}</h3>
            </div>
        );
    }
}

export default HelloWorldCard