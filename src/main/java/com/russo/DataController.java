package com.russo;

import java.time.LocalTime;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;

@RestController
public class DataController {
    
    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Data
    static class Result {
        private final int left;
        private final int right;
        private final long answer;

        public Result(final int left, final int right, final long answer) {
            this.left = left;
            this.right = right;
            this.answer = answer;
        }
    }

    @Data
    static class HelloResult {
        private final String data1;
        private final String data2;
        private final String data3;

        public HelloResult() {
            LocalTime time;

            time = LocalTime.now();

            this.data1 = "Good job!";
            this.data2 = "If you're seeing this message, that means everything worked as expected!";
            this.data3 = time.toString();
        }
    }

    // SQL sample
    @GetMapping("calc")
    Result calc(@RequestParam final int left, @RequestParam final int right) {
        final MapSqlParameterSource source = new MapSqlParameterSource().addValue("left", left).addValue("right",
                right);
        return jdbcTemplate.queryForObject("SELECT :left + :right AS answer", source,
                (rs, rowNum) -> new Result(left, right, rs.getLong("answer")));
    }

    @GetMapping("api")
    HelloResult getSampleMessage(final HttpServletResponse response) {
        return new HelloResult();
    }
}