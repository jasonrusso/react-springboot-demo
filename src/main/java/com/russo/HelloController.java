package com.russo;

import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

    @RequestMapping("/index")
    public String hello(HttpServletResponse response) {
        response.setHeader("Content-Type", "text/html");
        return "index.html";
    }
}
