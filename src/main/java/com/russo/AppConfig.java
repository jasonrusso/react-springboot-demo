package com.russo;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
@PropertySource(value = "classpath:application.yml")
public class AppConfig {
    @Autowired
    DataSourceProperties dataSourceProperties;

    @Bean
    ServletWebServerFactory servletWebServerFactory() {
        return new TomcatServletWebServerFactory();
    }
    
    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder emdbBuilder;
        EmbeddedDatabase emdb;

        emdbBuilder = new EmbeddedDatabaseBuilder();
        emdb = emdbBuilder.setType(EmbeddedDatabaseType.H2).build();
        return emdb;
    }
}